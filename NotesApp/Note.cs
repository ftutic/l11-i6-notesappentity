﻿namespace NotesApp
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Note
    {

   
        public int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [Column(TypeName = "text")]
        public string Content { get; set; }

        public override string ToString()
        {
            int ContentLength;
            if (Content == null) ContentLength = 0;
            else ContentLength = Content.Length;
            return string.Format("{0} ({1} znakova)",Name, ContentLength);
        }
    }
}
