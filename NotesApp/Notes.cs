﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace NotesApp {
  // MORA SADRŽAVATI REFERENCU NA CONTEXT

  public class Notes
    {
    private NotesAppContext db;


        public Notes() {
      db = new NotesAppContext();
    }

    public IEnumerable<Note> GetNotes() {
      return
        from n in db.Notes
        orderby n.Name
        select n;
    }

    public Note GetNote(int id) {
      var note =
        from n in db.Notes
        where n.Id == id
        select n;

      if (!note.Any()) {
        return null;
      }

      return note.First();
    }

    public Note GetNullFutureNote() {
            Note note = new Note();
            var newId = 1;
            try
            {
                newId = db.Notes.Max(entity => entity.Id) + 1;
            }
            catch { };
            note.Id = newId;
            note.Content = "";
            note.Name = "";
            return note;
        }

    public bool DeleteNote(Note note) {
            // delete note code goes here
            try { 
    var noteToDelete = db.Notes.Where(n => n.Id == note.Id).First();
    db.Notes.Remove(noteToDelete);
            }
            catch { 
            return false;
            };
    db.SaveChangesAsync();
            return true;
    }

    public void UpdateNote(Note note) {
      // update note code goes here
      db.Notes.AddOrUpdate(note);
      db.SaveChangesAsync();
    }
  }
}
